import { Module } from '@nestjs/common'
import { dataBaseProviders } from './database.providers'

@Module({
  components: [...dataBaseProviders],
  exports: [...dataBaseProviders]
})

export class DataBaseModule {}