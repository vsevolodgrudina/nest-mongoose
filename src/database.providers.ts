import * as mongoose from 'mongoose';
import config from './config'

export const dataBaseProviders = [
  {
    provide: 'DbConnectionToken',
    useFactory: async () => {
      (mongoose as any).Promise = global.Promise;
      return await mongoose.connect(config.mongo, {
        useMongoClient: true,
      });
    }
  }
]