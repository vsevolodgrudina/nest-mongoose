export default {
  prodPort: process.env.PORT,
  devPort: 3000,
  debug: true,
  mongo: 'mongodb://localhost/heroesData'
}