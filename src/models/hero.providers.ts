import { Connection } from 'mongoose'
import { heroSchema } from './hero.schema'

export const catsProviders = [
  {
    provide: 'HeroModelToken',
    useFactory: (connection: Connection) => connection.model('heroes', heroSchema),
    inject: ['DbConnectionToken'],
  },
];