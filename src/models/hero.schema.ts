import * as mongoose from 'mongoose';

const { ObjectId } = mongoose.Schema.Types

export const heroSchema = new mongoose.Schema({
  _id: ObjectId,
  name: String,
  sureName: String,
  createdAt: Date,
})