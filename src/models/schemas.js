import mongoose from 'mongoose'
import config from '../config'

mongoose.Promise = global.Promise
mongoose.connect(config.mongo)
console.log(mongoose)

const { ObjectId } = mongoose.Schema.Types

export const hero = new mongoose.Schema({
  _id: ObjectId
  name: String,
  sureName: String,
  createdAt: Date,
})

export const message = new mongoose.Schema({
  _id: ObjectId,
  text: String,
  heroId: Number,
  createdAt: Date
})