import mongoose from 'mongoose'
import { hero } from './schemas'

export const heroSchema = mongoose.model('hero', hero)