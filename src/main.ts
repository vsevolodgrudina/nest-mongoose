import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app.module';
import config from './config';

async function bootstrap() {
	const { devPort, prodPort } = config;
	const port = process.env.PRODUCTION ? prodPort : devPort
	const app = await NestFactory.create(ApplicationModule);
	await app.listen(+port);
}
bootstrap();
